<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }
    public function welcome(){
        return view('welcome');
    }
    public function form(){
        $name = request('firstname') . " " . request('lastname');
        return view('welcome', compact('name'));
    }
}
