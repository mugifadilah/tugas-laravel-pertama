<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Form Pendaftaran</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="welcome" method="POST">
      @csrf
      <label>First Name:</label><br /><br />
      <input type="text" name="firstname" /><br /><br />
      <label>Last Name:</label><br /><br />
      <input type="text" name="lastname" /><br /><br />
      <label>Gender:</label><br /><br />
      <input type="radio" name="gender" value="male" />Male<br />
      <input type="radio" name="gender" value="female" />Female<br />
      <input type="radio" name="gender" value="other" />Other<br /><br />
      <label>Nationality:</label><br /><br />
      <select name="negara">
        <option value="id">Indonesian</option>
        <option value="sg">Singapore</option>
        <option value="my">Malaysian</option>
        <option value="jp">Japan</option></select
      ><br /><br />
      <label>Language Spoken:</label><br /><br />
      <input type="checkbox" />Bahasa Indonesia<br />
      <input type="checkbox" />English<br />
      <input type="checkbox" />Other<br /><br />
      <label>Bio:</label><br /><br />
      <textarea cols="30" rows="10" name="bio"></textarea><br /><br />
      <input type="submit" value="Sign Up" />
    </form>
  </body>
</html>